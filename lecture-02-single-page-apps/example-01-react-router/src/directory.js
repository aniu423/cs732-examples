import React from 'react';
import styles from './master-detail.module.css';
import StaffDetails from './staff-details';
import {NavLink, withRouter} from 'react-router-dom';

class Directory extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            staff: [
                {id: 1, name: "Ash Ketchum", department: "Training", phNumber: "021 1234567", email: "ash@silphco.biz"},
                {id: 2, name: "Misty", department: "Leadership", phNumber: "021 1549810", email: "misty@silphco.biz"},
                {id: 3, name: "Brock", department: "Leadership", phNumber: "021 9876543", email: "brock@silphco.biz"},
                {id: 4, name: "May", department: "Training", phNumber: "021 9048378", email: "may@silphco.biz"}
            ]
        };
    }

    /**
     * Complete this as a class exercise. Links should be displayed in the <aside> - one for each staff member.
     * When clicked, the user should be routed to an appropriate URL for that staff member - whose details should appear
     * in the <main>. You may display a staff member using the <StaffDetails> component.
     */
    render() {
        const {path, url, params} = this.props.match;
        console.log(this.props.match);

        return (
            <div className={styles.container}>
                <aside>
                    {this.state.staff.map((item, index) => (
                        <NavLink to={`/staff-directory/${item.id}`} activeClassName={styles.activeLink}
                                 key={index}>{item.name}</NavLink>))}
                </aside>
                {(params.id) ?
                    <main>
                        {console.log("url: " + url)}
                        {console.log("path: " + path)}
                        {console.log(this.props.match)}
                        <StaffDetails staffMember={this.state.staff.find((staff) => staff.id == params.id)}/>
                    </main> : <main></main>
                }


            </div>
        );
    }

}

export default withRouter(Directory);
